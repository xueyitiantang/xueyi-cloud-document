# 介绍

<h1 align="center">XueYi</h1>

<p align="center">
    <a>
       <img src="https://img.shields.io/badge/XueYi--MultiSaas-v1.0.0-brightgreen" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/SpringBoot-2.5.6-brightgreen" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/Spring%20Cloud%20%26%20Alibaba-2021.1-brightgreen" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/Vue3-Ant--Design--Vue-green" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/vite2-TypeScript-green" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/Mybatis--Plus-3.4.0%2B-brightgreen" alt="xueYi-cloud">
    </a>
</p>

**XueYi | 雪忆微服务系统** 是一个 Java EE 企业级微服务开发框架，基于经典技术组合（SpringBoot | Spring Cloud & Alibaba | Mybatis-Plus | Vue3 | vite2 | TypeScript | Ant-Design-Vue），内置模块如：机构管理（部门-岗位-用户）、权限管理（模块-菜单-角色）、素材管理中心、系统参数、日志管理、代码生成等。在线定时任务配置等。

**在线体验**

先占位，待开放

[comment]: <> (1. 项目地址：[https://xueyitt.cn]&#40;https://xueyitt.cn&#41;)

[comment]: <> (2. 普通账号：xueYi/admin/admin123)

[comment]: <> (2. 租管账号：administrator/admin/admin123)

**系统需求**

- JDK >= 1.8
- MySQL >= 8.0
- Maven >= 3.0

**技术交流群**
- [![加入QQ群](https://img.shields.io/badge/779343138-blue.svg)](https://jq.qq.com/?_wv=1027&k=zw11JJhj)
