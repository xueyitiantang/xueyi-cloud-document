# 后台手册

## 分页实现

前端基于Element封装的分页组件 `Pagination`  
后端分页组件使用Mybatis分页插件 `PageHelper`

### 前端调用实现

1、前端定义分页流程
```javascript
// 一般在查询参数中定义分页变量
queryParams: {
  pageNum: 1,
  pageSize: 10
},

// 页面添加分页组件，传入分页变量
<pagination
  v-show="total>0"
  :total="total"
  :page.sync="queryParams.pageNum"
  :limit.sync="queryParams.pageSize"
  @pagination="getList"
/>

// 调用后台方法，传入参数 获取结果
listUser(this.queryParams).then(response => {
    this.userList = response.rows;
    this.total = response.total;
  }
);
```

### 后台逻辑实现

```java
@PostMapping("/list")
@ResponseBody
public TableDataInfo list(User user)
{
    startPage();  // 此方法配合前端完成自动分页
    List<User> list = userService.selectUserList(user);
    return getDataTable(list);
}
```

常见坑点1：`selectPostById`莫名其妙的分页。例如下面这段代码
```java
startPage();
List<User> list;
if(user != null){
    list = userService.selectUserList(user);
} else {
    list = new ArrayList<User>();
}
Post post = postService.selectPostById(1L);
return getDataTable(list);
```

原因分析：这种情况下由于`user`存在`null`的情况，就会导致`pageHelper`生产了一个分页参数，但是没有被消费，
这个参数就会一直保留在这个线程上。 当这个线程再次被使用时，就可能导致不该分页的方法去消费这个分页参数，
这就产生了莫名其妙的分页。
上面这个代码，应该写成下面这个样子才能保证安全。
```java
List<User> list;
if(user != null){
	startPage();
	list = userService.selectUserList(user);
} else {
	list = new ArrayList<User>();
}
Post post = postService.selectPostById(1L);
return getDataTable(list);
```

常见坑点2：添加了`startPage`方法。也没有正常分页。例如下面这段代码
```java
startPage();
Post post = postService.selectPostById(1L);
List<User> list = userService.selectUserList(user);
return getDataTable(list);
```

原因分析：只对该语句以后的第一个查询（`Select`）语句得到的数据进行分页。  
上面这个代码，应该写成下面这个样子才能正常分页。
```java
Post post = postService.selectPostById(1L);
startPage();
List<User> list = userService.selectUserList(user);
return getDataTable(list);
```

::: tip 注意
如果改为其他数据库需修改配置`application.yml`文件中的属性`helperDialect=你的数据库`
:::

## 导入导出

在实际开发中经常需要使用导入导出功能来加快数据的操作。在项目中可以使用注解来完成此项功能。
在需要被导入导出的实体类属性添加`@Excel`注解，目前支持参数如下：

### 注解参数说明  
| 参数                           | 类型                                  | 默认值                       | 描述                                                       |
| ------------------------------ | ------------------------------------- | ---------------------------- | ----------------------------------------------------------|
| sort                      	 | int	                                 | Integer.MAX_VALUE	        | 导出时在excel中排序，值越小越靠前
| name	                         | String	                             | 空	                        | 导出到Excel中的名字                                 
| dateFormat	                 | String	                             | 空	                        | 日期格式, 如: yyyy-MM-dd
| readConverterExp	             | String	                             | 空	                        | 读取内容转表达式 (如: 0=男,1=女,2=未知)
| separator	                     | String                             	 | ,	                        | 分隔符，读取字符串组内容
| scale	                         | int	                                 | -1	                        | BigDecimal 精度 默认:-1(默认不开启BigDecimal格式化)
| roundingMode	                 | int	                                 | BigDecimal.ROUND_HALF_EVEN	| BigDecimal 舍入规则 默认:BigDecimal.ROUND_HALF_EVEN
| columnType	                 | Enum                                  | Type.STRING	                | 导出类型（0数字 1字符串 2图片）
| height	                     | String	                             | 14	                        | 导出时在excel中每个列的高度 单位为字符
| width	                         | String	                             | 16	                        | 导出时在excel中每个列的宽 单位为字符
| suffix	                     | String	                             | 空	                        | 文字后缀,如% 90 变成90%
| defaultValue	                 | String	                             | 空	                        | 当值为空时,字段的默认值
| prompt	                     | String                             	 | 空	                        | 提示信息
| combo	                         | String                             	 | Null	                        | 设置只能选择不能输入的列内容
| targetAttr	                 | String                             	 | 空	                        | 另一个类中的属性名称,支持多级获取,以小数点隔开
| isStatistics	                 | boolean	                             | false	                    | 是否自动统计数据,在最后追加一行统计数据总和
| type	                         | Enum	                                 | Type.ALL	                    | 字段类型（0：导出导入；1：仅导出；2：仅导入）
| align	                         | Enum	                                 | Type.AUTO	                | 导出字段对齐方式（0：默认；1：靠左；2：居中；3：靠右）
| handler	                     | Class	                             | ExcelHandlerAdapter.class	| 自定义数据处理器
| args	                         | String[]	                             | {}	                        | 自定义数据处理器参数

### 导出实现流程

1、前端调用方法（参考如下）
```javascript
// 查询参数 queryParams
queryParams: {
    pageNum: 1,
    pageSize: 10,
    userName: undefined
},

/** 导出按钮操作 */
handleExport() {
    this.download('system/xxxx/export', {
        ...this.queryParams
    }, `post_${new Date().getTime()}.xlsx`)
}
```

2、添加导出按钮事件
```html
<el-button
    type="warning"
    icon="el-icon-download"
    size="mini"
    @click="handleExport"
>导出</el-button>
```

3、在实体变量上添加`@Excel`注解
```java
@Excel(name = "用户序号", prompt = "用户编号")
private Long userId;

@Excel(name = "用户名称")
private String userName;
	
@Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
private String sex;

@Excel(name = "最后登陆时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
private Date loginDate;
```

4、在`Controller`添加导出方法
```java
@Log(title = "用户管理", businessType = BusinessType.EXPORT)
@RequiresPermissions("system:user:export")
@PostMapping("/export")
public void export(HttpServletResponse response, SysUser user) throws IOException
{
	List<SysUser> list = userService.selectUserList(user);
	ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
	util.exportExcel(response, list, "用户数据");
}
```

### 导入实现流程

1、前端调用方法（参考如下）
```javascript
import { getToken } from "@utils/auth";

// 用户导入参数
upload: {
    // 是否显示弹出层（用户导入）
    open: false, 
    // 弹出层标题（用户导入）
    title: "",
    // 是否禁用上传
    isUploading: false,
    // 是否更新已经存在的用户数据
    updateSupport: 0,
    // 设置上传的请求头部
    headers: { Authorization: "Bearer " + getToken() },
    // 上传的地址
    url: process.env.VUE_APP_BASE_API + "/system/user/importData"
},

// 导入模板接口importTemplate
import { importTemplate } from "@/api/system/user";

/** 导入按钮操作 */
handleImport() {
    this.upload.title = "用户导入";
    this.upload.open = true;
},
/** 下载模板操作 */
importTemplate() {
    importTemplate().then(response => {
        this.download(response.msg);
    });
},
// 文件上传中处理
handleFileUploadProgress(event, file, fileList) {
    this.upload.isUploading = true;
},
// 文件上传成功处理
handleFileSuccess(response, file, fileList) {
    this.upload.open = false;
    this.upload.isUploading = false;
    this.$refs.upload.clearFiles();
    this.$alert(response.msg, "导入结果", { dangerouslyUseHTMLString: true });
    this.getList();
},
// 提交上传文件
submitFileForm() {
    this.$refs.upload.submit();
}
```

2、添加导入按钮事件
```html
<el-button
  type="info"
  icon="el-icon-upload2"
  size="mini"
  @click="handleImport"
>导入</el-button>
```

3、添加导入前端代码
```html
<!-- 用户导入对话框 -->
<el-dialog :title="upload.title" :visible.sync="upload.open" width="400px">
  <el-upload
	ref="upload"
	:limit="1"
	accept=".xlsx, .xls"
	:headers="upload.headers"
	:action="upload.url + '?updateSupport=' + upload.updateSupport"
	:disabled="upload.isUploading"
	:on-progress="handleFileUploadProgress"
	:on-success="handleFileSuccess"
	:auto-upload="false"
	drag
  >
	<i class="el-icon-upload"></i>
	<div class="el-upload__text">
	  将文件拖到此处，或
	  <em>点击上传</em>
	</div>
	<div class="el-upload__tip" slot="tip">
	  <el-checkbox v-model="upload.updateSupport" />是否更新已经存在的用户数据
	  <el-link type="info" style="font-size:12px" @click="importTemplate">下载模板</el-link>
	</div>
	<div class="el-upload__tip" style="color:red" slot="tip">提示：仅允许导入“xls”或“xlsx”格式文件！</div>
  </el-upload>
  <div slot="footer" class="dialog-footer">
	<el-button type="primary" @click="submitFileForm">确 定</el-button>
	<el-button @click="upload.open = false">取 消</el-button>
  </div>
</el-dialog>
```

4、在实体变量上添加`@Excel`注解，默认为导出导入，也可以单独设置仅导入`Type.IMPORT`
```java
@Excel(name = "用户序号")
private Long id;

@Excel(name = "部门编号", type = Type.IMPORT)
private Long deptId;

@Excel(name = "用户名称")
private String userName;

/** 导出部门多个对象 */
@Excels({
	@Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT),
	@Excel(name = "部门负责人", targetAttr = "leader", type = Type.EXPORT)
})
private SysDept dept;

/** 导出部门单个对象 */
@Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT)
private SysDept dept;
```

5、在`Controller`添加导入方法，`updateSupport`属性为是否存在则覆盖（可选）
```java
@Log(title = "用户管理", businessType = BusinessType.IMPORT)
@PostMapping("/importData")
public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
	ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
	List<SysUser> userList = util.importExcel(file.getInputStream());
	LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
	String operName = loginUser.getUsername();
	String message = userService.importUser(userList, updateSupport, operName);
	return AjaxResult.success(message);
}

@GetMapping("/importTemplate")
public AjaxResult importTemplate() {
	ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
	return util.importTemplateExcel("用户数据");
}
```

::: tip 提示
也可以直接到main运行此方法测试。  
```java
InputStream is = new FileInputStream(new File("D:\\test.xlsx"));
ExcelUtil<Entity> util = new ExcelUtil<Entity>(Entity.class);
List<Entity> userList = util.importExcel(is);
```
:::

### 自定义标题信息
有时候我们希望导出表格包含标题信息，我们可以这样做。  
导出用户管理表格新增标题（用户列表）  
```java
public AjaxResult export(SysUser user) {
	List<SysUser> list = userService.selectUserList(user);
	ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
	return util.exportExcel(list, "用户数据", "用户列表");
}
```
导入表格包含标题处理方式，其中`1`表示标题占用行数，根据实际情况填写。  
```java
public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
	ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
	List<SysUser> userList = util.importExcel(file.getInputStream(), 1);
	String operName = SecurityUtils.getUsername();
	String message = userService.importUser(userList, updateSupport, operName);
	return AjaxResult.success(message);
}
```
### 自定义数据处理器
有时候我们希望数据展现为一个特殊的格式，或者需要对数据进行其它处理。`Excel`注解提供了自定义数据处理器以满足各种业务场景。而实现一个数据处理器也是非常简单的。如下：   

1、在实体类用`Excel`注解`handler`属性指定自定义的数据处理器  
```java
public class User extends BaseEntity {
    @Excel(name = "用户名称", handler = MyDataHandler.class, args = { "aaa", "bbb" })
    private String userName;
}
```
2、编写数据处理器`MyDataHandler`继承`ExcelHandlerAdapter`，返回值为处理后的值。  
```java 
public class MyDataHandler implements ExcelHandlerAdapter {

    @Override
    public Object format(Object value, String[] args) {
        // value 为单元格数据值
		// args 为excel注解args参数组
		return value;
    }
}
```

## 上传下载

首先创建一张上传文件的表，例如：
```sql
drop table if exists sys_file_info;
create table sys_file_info (
  file_id           int(11)          not null auto_increment       comment '文件id',
  file_name         varchar(50)      default ''                    comment '文件名称',
  file_path         varchar(255)     default ''                    comment '文件路径',
  primary key (file_id)
) engine=innodb auto_increment=1 default charset=utf8 comment = '文件信息表';
```

### 上传实现流程

1、`el-input`修改成`el-upload`

```vue
<el-upload
    ref="upload"
    :limit="1"
    accept=".jpg, .png"
    :action="upload.url"
    :headers="upload.headers"
    :file-list="upload.fileList"
    :on-progress="handleFileUploadProgress"
    :on-success="handleFileSuccess"
    :auto-upload="false">
<el-button slot="trigger" size="small" type="primary">选取文件</el-button>
<el-button style="margin-left: 10px;" size="small" type="success" :loading="upload.isUploading" @click="submitUpload">上传到服务器</el-button>
<div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且不超过500kb</div>
</el-upload>
```

2、引入获取token

```javascript
import { getToken } from "@utils/auth";
```

3、`data`中添加属性

```javascript
// 上传参数
upload: {
    // 是否禁用上传
    isUploading: false,
    // 设置上传的请求头部
    headers: { Authorization: "Bearer " + getToken() },
    // 上传的地址
    url: process.env.VUE_APP_BASE_API + "/common/upload",
    // 上传的文件列表
    fileList: []
},
```

4、新增和修改操作对应处理`fileList`参数

```javascript
handleAdd() {
    ...
    this.upload.fileList = [];
}

handleUpdate(row) {
    ...
    this.upload.fileList = [{ name: this.form.fileName, url: this.form.filePath }];
}
```

5、添加对应事件

```javascript
// 文件提交处理
submitUpload() {
    this.$refs.upload.submit();
},
// 文件上传中处理
handleFileUploadProgress(event, file, fileList) {
    this.upload.isUploading = true;
},
// 文件上传成功处理
handleFileSuccess(response, file, fileList) {
    this.upload.isUploading = false;
    this.form.filePath = response.url;
    this.msgSuccess(response.msg);
}
```

### 下载实现流程

1、添加对应按钮和事件

```vue
<el-button
  size="mini"
  type="text"
  icon="el-icon-edit"
  @click="handleDownload(scope.row)"
>下载</el-button>
```
		  
2、实现文件下载

```javascript
// 文件下载处理
handleDownload(row) {
  var name = row.fileName;
  var url = row.filePath;
  var suffix = url.substring(url.lastIndexOf("."), url.length);
  const a = document.createElement('a')
  a.setAttribute('download', name + suffix)
  a.setAttribute('target', '_blank')
  a.setAttribute('href', url)
  a.click()
}
```

## 权限注解

1) 数据权限示例。
```java
// 符合system:user:list权限要求
@PreAuthorize("@ss.hasPermi('system:user:list')")

// 不符合system:user:list权限要求
@PreAuthorize("@ss.lacksPermi('system:user:list')")

// 符合system:user:add或system:user:edit权限要求即可
@PreAuthorize("@ss.hasAnyPermi('system:user:add,system:user:edit')")
```

2) 角色权限示例。
```java
// 属于user角色
@PreAuthorize("@ss.hasRole('user')")

// 不属于user角色
@PreAuthorize("@ss.lacksRole('user')")

// 属于user或者admin之一
@PreAuthorize("@ss.hasAnyRoles('user,admin')")
```

## 事务管理

新建的`Spring Boot`项目中，一般都会引用`spring-boot-starter`或者`spring-boot-starter-web`，
而这两个起步依赖中都已经包含了对于`spring-boot-starter-jdbc`或`spring-boot-starter-data-jpa`的依赖。
当我们使用了这两个依赖的时候，框架会自动默认分别注入`DataSourceTransactionManager`或`JpaTransactionManager`。
所以我们不需要任何额外配置就可以用`@Transactional`注解进行事务的使用。

::: tip 提示
`@Transactional`注解只能应用到`public`可见度的方法上，可以被应用于接口定义和接口方法，方法会覆盖类上面声明的事务。
:::

例如用户新增需要插入用户表、用户与岗位关联表、用户与角色关联表，如果插入成功，那么一起成功，如果中间有一条出现异常，
那么回滚之前的所有操作，这样可以防止出现脏数据，就可以使用事务让它实现回退。  
做法非常简单，我们只需要在方法或类添加`@Transactional`注解即可。

```java
@Transactional
public int insertUser(User user) {
	// 新增用户信息
	int rows = userMapper.insertUser(user);
	// 新增用户岗位关联
	insertUserPost(user);
	// 新增用户与角色管理
	insertUserRole(user);
	return rows;
}
```

* 常见坑点1：遇到检查异常时，事务开启，也无法回滚。     
例如下面这段代码，用户依旧增加成功，并没有因为后面遇到检查异常而回滚！！
```java
@Transactional
public int insertUser(User user) throws Exception {
	// 新增用户信息
	int rows = userMapper.insertUser(user);
	// 新增用户岗位关联
	insertUserPost(user);
	// 新增用户与角色管理
	insertUserRole(user);
	// 模拟抛出SQLException异常
	boolean flag = true;
	if (flag) {
		throw new SQLException("发生异常了..");
	}
	return rows;
}
```
原因分析：因为`Spring`的默认的事务规则是遇到运行异常`（RuntimeException）`和程序错误`（Error）`才会回滚。
如果想针对检查异常进行事务回滚，可以在`@Transactional`注解里使用`rollbackFor`属性明确指定异常。  
例如下面这样，就可以正常回滚：
```java
@Transactional(rollbackFor = Exception.class)
public int insertUser(User user) throws Exception {
	// 新增用户信息
	int rows = userMapper.insertUser(user);
	// 新增用户岗位关联
	insertUserPost(user);
	// 新增用户与角色管理
	insertUserRole(user);
	// 模拟抛出SQLException异常
	boolean flag = true;
	if (flag) {
		throw new SQLException("发生异常了..");
	}
	return rows;
}
```

* 常见坑点2： 在业务层捕捉异常后，发现事务不生效。这是许多新手都会犯的一个错误，在业务层手工捕捉并处理了异常，你都把异常“吃”掉了，Spring自然不知道这里有错，更不会主动去回滚数据。  
例如：下面这段代码直接导致用户新增的事务回滚没有生效。
```java
@Transactional
public int insertUser(User user) throws Exception{
	// 新增用户信息
	int rows = userMapper.insertUser(user);
	// 新增用户岗位关联
	insertUserPost(user);
	// 新增用户与角色管理
	insertUserRole(user);
	// 模拟抛出SQLException异常
	boolean flag = true;
	if (flag){
		try{
			// 谨慎：尽量不要在业务层捕捉异常并处理
			throw new SQLException("发生异常了..");
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	return rows;
}
```

推荐做法：在业务层统一抛出异常，然后在控制层统一处理。
```java
@Transactional
public int insertUser(User user) throws Exception{
	// 新增用户信息
	int rows = userMapper.insertUser(user);
	// 新增用户岗位关联
	insertUserPost(user);
	// 新增用户与角色管理
	insertUserRole(user);
	// 模拟抛出SQLException异常
	boolean flag = true;
	if (flag){
		throw new RuntimeException("发生异常了..");
	}
	return rows;
}
```

`Transactional`注解的常用属性表：

| 属性                           | 说明                                  | 
| ------------------------------ | ------------------------------------- |
| propagation                    | 事务的传播行为，默认值为 REQUIRED。   | 
| isolation                      | 事务的隔离度，默认值采用 DEFAULT      | 
| timeout                        | 事务的超时时间，默认值为-1，不超时。如果设置了超时时间(单位秒)，那么如果超过该时间限制了但事务还没有完成，则自动回滚事务。       | 
| read-only                      | 指定事务是否为只读事务，默认值为 false；为了忽略那些不需要事务的方法，比如读取数据，可以设置 read-only 为 true。                 | 
| rollbackFor                    | 用于指定能够触发事务回滚的异常类型，如果有多个异常类型需要指定，各类型之间可以通过逗号分隔。{xxx1.class, xxx2.class,……}          |
| noRollbackFor                  | 抛出 no-rollback-for 指定的异常类型，不回滚事务。{xxx1.class, xxx2.class,……}                                                     | 
|.... |

::: tip 提示
事务的传播机制是指如果在开始当前事务之前，一个事务上下文已经存在，此时有若干选项可以指定一个事务性方法的执行行为。
即:在执行一个`@Transactinal`注解标注的方法时，开启了事务；当该方法还在执行中时，另一个人也触发了该方法；那么此时怎么算事务呢，这时就可以通过事务的传播机制来指定处理方式。
:::

`TransactionDefinition`传播行为的常量：

| 常量                                               | 含义                                                                                  |
| -------------------------------------------------- | ------------------------------------------------------------------------------------- |
| TransactionDefinition.PROPAGATION_REQUIRED         | 如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。这是默认值。    |
| TransactionDefinition.PROPAGATION_REQUIRES_NEW     | 创建一个新的事务，如果当前存在事务，则把当前事务挂起。                                |
| TransactionDefinition.PROPAGATION_SUPPORTS         | 如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。          |
| TransactionDefinition.PROPAGATION_NOT_SUPPORTED    | 以非事务方式运行，如果当前存在事务，则把当前事务挂起。                                |
| TransactionDefinition.PROPAGATION_NEVER            | 以非事务方式运行，如果当前存在事务，则抛出异常。                                      |
| TransactionDefinition.PROPAGATION_MANDATORY        | 如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。                        |
| TransactionDefinition.PROPAGATION_NESTED           | 如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于TransactionDefinition.PROPAGATION_REQUIRED。 |


## 异常处理

通常一个`web`框架中，有大量需要处理的异常。比如业务异常，权限不足等等。前端通过弹出提示信息的方式告诉用户出了什么错误。
通常情况下我们用`try.....catch....`对异常进行捕捉处理，但是在实际项目中对业务模块进行异常捕捉，会造成代码重复和繁杂，
我们希望代码中只有业务相关的操作，所有的异常我们单独设立一个类来处理它。全局异常就是对框架所有异常进行统一管理。
我们在可能发生异常的方法里`throw`抛给控制器。然后由全局异常处理器对异常进行统一处理。
如此，我们的`Controller`中的方法就可以很简洁了。

所谓全局异常处理器就是使用`@ControllerAdvice`注解。示例如下：

1、统一返回实体定义
```java
package com.xueyi.common.core.domain;

import java.util.HashMap;

/**
 * 操作消息提醒
 * @author xueyi
 */
public class AjaxResult extends HashMap<String, Object>{

    private static final long serialVersionUID = 1L;

    /**
     * 返回错误消息
     * 
     * @param code 错误码
     * @param msg 内容
     * @return 错误消息
     */
    public static AjaxResult error(String msg){
        AjaxResult json = new AjaxResult();
        json.put("msg", msg);
        json.put("code", 500);
        return json;
    }

    /**
     * 返回成功消息
     * 
     * @param msg 内容
     * @return 成功消息
     */
    public static AjaxResult success(String msg){
        AjaxResult json = new AjaxResult();
        json.put("msg", msg);
        json.put("code", 0);
        return json;
    }
}
```

2、定义登录异常定义
```java
package com.xueyi.common.exception;

/**
 * 登录异常
 * @author xueyi
 */
public class LoginException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    protected final String message;

    public LoginException(String message){
        this.message = message;
    }

    @Override
    public String getMessage(){
        return message;
    }
}

```

3、基于`@ControllerAdvice`注解的`Controller`层的全局异常统一处理
```java
package com.xueyi.framework.web.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.xueyi.common.core.domain.AjaxResult;
import com.xueyi.common.exception.LoginException;

/**
 * 全局异常处理器
 */
@RestControllerAdvice
public class GlobalExceptionHandler{

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	/**
     * 登录异常
     */
    @ExceptionHandler(LoginException.class)
    public AjaxResult loginException(LoginException e){
        log.error(e.getMessage(), e);
        return AjaxResult.error(e.getMessage());
    }
}
```

4、测试访问请求
```java
@Controller
public class SysIndexController {
    /**
     * 首页方法
     */
    @GetMapping("/index")
    public String index(ModelMap mmap){
    
        /**
         * 模拟用户未登录，抛出业务逻辑异常
         */
        SysUser user = ShiroUtils.getSysUser();
        if (StringUtils.isNull(user)){
            throw new LoginException("用户未登录，无法访问请求。");
        }
		mmap.put("user", user);
        return "index";
    }
}
```

根据上面代码含义，当我们未登录访问`/index`时就会发生`LoginException`业务逻辑异常，按照我们之前的全局异常配置以及统一返回实体实例化，访问后会出现`AjaxResult`格式`JSON`数据，
下面我们运行项目访问查看效果。  
界面输出内容如下所示：
```json
{
    "msg": "用户未登录，无法访问请求。",
    "code": 500
}
```

对于一些特殊情况，如接口需要返回`json`，页面请求返回`html`可以使用如下方法：
```java
@ExceptionHandler(LoginException.class)
public Object loginException(HttpServletRequest request, LoginException e){
	log.error(e.getMessage(), e);

	if (ServletUtils.isAjaxRequest(request)){
		return AjaxResult.error(e.getMessage());
	}else{
		return new ModelAndView("/error/500");
	}
}
```

本系统的全局异常处理器`GlobalExceptionHandler`  
注意：如果全部异常处理返回`json`，那么可以使用`@RestControllerAdvice`代替`@ControllerAdvice`，这样在方法上就可以不需要添加`@ResponseBody`。

# 参数验证
`spring boot`中可以用`@Validated`来校验数据，如果数据异常则会统一抛出异常，方便异常中心统一处理。

### 注解参数说明
  | 注解名称                      | 功能                                                    |
  | --------------------------- | ------------------------------------------------------- |
  | @Null	                    | 检查该字段为空
  | @NotNull	                | 不能为null
  | @NotBlank	                | 不能为空，常用于检查空字符串
  | @NotEmpty	                | 不能为空，多用于检测list是否size是0
  | @Max	                    | 该字段的值只能小于或等于该值
  | @Min	                    | 该字段的值只能大于或等于该值
  | @Past	                    | 检查该字段的日期是在过去
  | @Future	                    | 检查该字段的日期是否是属于将来的日期
  | @Email	                    | 检查是否是一个有效的email地址
  | @Pattern(regex=,flag=)	    | 被注释的元素必须符合指定的正则表达式
  | @Range(min=,max=,message=)	| 被注释的元素必须在合适的范围内
  | @Size(min=, max=)	        | 检查该字段的size是否在min和max之间，可以是字符串、数组、集合、Map等
  | @Length(min=,max=)	        | 检查所属的字段的长度是否在min和max之间,只能用于字符串
  | @AssertTrue	                | 用于boolean字段，该字段只能为true
  | @AssertFalse	            | 该字段的值只能为false

### 数据校验使用
1、基础使用 因为`spring boot`已经引入了基础包，所以直接使用就可以了。首先在`controller`上声明`@Validated`需要对数据进行校验。

```java
public AjaxResult add(@Validated @RequestBody SysUser user){
    .....
}
```

2、然后在对应字段`Get方法`加上参数校验注解，如果不符合验证要求，则会以`message`的信息为准，返回给前端。
```java
@Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
public String getNickName(){
	return nickName;
}

@NotBlank(message = "用户账号不能为空")
@Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
public String getUserName(){
	return userName;
}

@Email(message = "邮箱格式不正确")
@Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
public String getEmail(){
	return email;
}

@Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
public String getPhonenumber(){
	return phonenumber;
}
```

::: tip 提示
也可以直接放在字段上面声明。
```java
@Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
private String nickName;
```
:::

## 系统日志

在实际开发中，对于某些关键业务，我们通常需要记录该操作的内容，一个操作调一次记录方法，每次还得去收集参数等等，会造成大量代码重复。
我们希望代码中只有业务相关的操作，在项目中使用注解来完成此项功能。

在需要被记录日志的`controller`方法上添加@Log注解，使用方法如下：
```java
@Log(title = "用户管理", businessType = BusinessType.INSERT)
public AjaxResult addSave(...){
    return success(...);
}
```

### 注解参数说明

| 参数                           | 类型                                  | 默认值                         | 描述                                                     |
| ------------------------------ | ------------------------------------- | ------------------------------ | -------------------------------------------------------- |
| title                          | String                                | 空                             | 操作模块                                                 |
| businessType                   | BusinessType                          | OTHER                          | 操作功能（`OTHER`其他 `INSERT`新增 `UPDATE`修改 `DELETE`删除 `GRANT`授权 `EXPORT`导出 `IMPORT`导入 `FORCE`强退 `GENCODE`生成代码 `CLEAN`清空数据）  |
| operatorType                   | OperatorType                          | MANAGE                         | 操作人类别（`OTHER`其他 `MANAGE`后台用户 `MOBILE`手机端用户）  |
| isSaveRequestData              | boolean                               | true                           | 是否保存请求的参数                                       |
| isSaveResponseData             | boolean                               | true                           | 是否保存响应的参数                                       |


### 自定义操作功能
1、在`BusinessType`中新增业务操作类型如:
```java
/**
 * 测试
 */
TEST,
```

2、在`sys_dict_data`字典数据表中初始化操作业务类型
```sql
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, create_by, remark) values (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', 0, '新增操作');
```

3、在`Controller`中使用注解
```java
@Log(title = "测试标题", businessType = BusinessType.TEST)
public AjaxResult test(...){
    return success(...);
}
```

逻辑实现代码[LogAspect.java](https://gitee.com/xueyitiantang/XueYi-Cloud/blob/master/xueyi-common/xueyi-common-log/src/main/java/com/xueyi/common/log/aspect/LogAspect.java)    
登录系统（系统管理-操作日志）可以查询操作日志列表和详细信息。   

## 租户隔离
在多租户SaaS架构中，存在五种分级模型：   

架构成熟度模型的5个级别——从“混乱”到“乌托邦“   

第0级（混乱）：每次新增一个客户，都会新增软件的一个实例。   
第1级（受控的混乱）：所有客户都运行在软件的同一个版本上，而且任何的定制化都通过修改配置来实现。    
第2级（多租户[multi-tenant]、高层建筑[Highrise]）：所有的客户都已经可以在软件的同一个版本上运行了，而且他们都在同一个“实例”上运行。   
第3级（多租户， 扩建[Build-Out]）：此时你已经拥有了多租户、单一版本的软件模型。不过你还是可以通过硬件扩展（scale-out）的方式来进行扩充。   
第4级（乌托邦）：如同第3级，除非你可以找出有效的方式，以在不同的“实例”上运行不同版本的软件。    

应用程序必须支持多租户：

多租户可以分为几个不同的类别：
1.1，云中的简单虚拟化，其中只对硬件进行共享。
1.2，共享应用程序，对每个租户使用不同的数据库。
1.3，共享应用程序和数据库（效率最高，真正的多租户）。

目前本系统可规划至第三级模型中，且已实现共享应用程序和数据库，同时各租户可进行额外系统接入。

### 逻辑隔离-初级版
在多租户SaaS中，对于不同租户的数据，它们是非常敏感的，由于存在应用程序和数据库共享，为保证各租户之间的数据保持无关联性，且租户与租户间数据不可进行跨级访问，故而，对租户数据进行逻辑隔离是必不可少的环节。

在理解本系统逻辑隔离的前提下，需先了解本系统中数据存在的两种形式：   
1.公共数据，对应数据表中`tenant_id = 0`的数据，这类数据所有用户都具备访问权限。    
2.私有数据，对应数据表中`tenant_id = 租户Id`的数据，这类数据只有当前所属租户具备访问权。   

egg:  
以菜单为例，假定当前有`a,b,c,d,e`五个菜单，`A,B`两个租户；在菜单开启了公共数据访问逻辑时，    
其中，菜单`a,b`属于租户`A`，菜单`c`属于租户`B`，菜单`d,e`为公共菜单；    
则，`A`可见菜单有`a,b,d,e`；      
`B`可见菜单有`c,d,e`。      

::: tip 提示
何为开启公共数据访问逻辑请看后续介绍，暂时你可以理解为一个开关，`if(打开) ? 可访问公共数据 : 不可访问公共数据`
:::

系统中提供三个默认租户控制参数（以租户的租户Id `tenantId = 1` 为例）

| 参数                           | 类型                                   | 默认值                         | 描述                                                      
| ----------------------------- | ------------------------------------- | -----------------------------  | ----------------------------------------------------------------------------------------------------------------------------------
| eAlias                        | String                                | 空                             | 租户表的别名（查询参数）（仅当前租户）（例如：当前`租户Id = 1`，通过本参数查询数据只能得到`租户Id = 1`的数据）
| edAlias                       | String                                | 空                             | 租户+系统基础表的别名（查询参数）（当前租户数据+通用租户数据）（例如：当前`租户Id = 1`，通过本参数查询数据可以得到`租户Id = 0 and 租户Id = 1`两个的数据）
| ueAlias                       | String                                | 空                             | 租户更新控制的别名（更新参数）（仅当前租户）（例如：当前`租户Id = 1`，通过本参数只能更新`租户Id = 1`的数据）

* `eAlias`、`edAlias` 参数用于数据查询操作
其中`eAlias`只能查询当前租户数据，例如：查询当前租户的部门数据、用户数据等（与其他租户无公共数据关联）
`edAlias`除查询当前租户数据外，还查询`租户Id = 0`的通用租户数据，例如：查询菜单、系统模块数据，（菜单、系统模块中大部分数据是所有租户通用的，但其中也有部分数据是仅当前租户的）
  
加`@DataScope`，且在`mybaties`中加入`${params.updateScope}`后，会进行如下释义:     
`eAlias`：在sql后加`(tenant_id = ?)`
`edAlias`：在sql后加`(tenant_id = ? or tenant_id = 0)`

2、在需要数据权限控制方法上添加`@DataScope`注解，其中`d`、`p`、`u`等为表别名，并在`mybatis`查询底部标签添加数据范围过滤

```java
// 租户表 权限注解
@DataScope(eAlias = "u")
// 租户+系统基础 权限注解
@DataScope(edAlias = "d")
// 租户更新控制 权限注解
@DataScope(ueAlias = "u")
```

```sql
//与 eAlias | edAlias 配套使用，用于查询控制
<!-- 数据范围过滤 -->
${params.dataScope}
```

```sql
//与 ueAlias 配套使用，用于更新控制
<!-- 数据范围过滤 -->
${params.updateScope}
```

用法案例：在`SysUserMapper`中有一个`selectUserList`方法，查询当前用户信息列表：
```java
public List<SysUser> selectUserList(SysUser sysUser);
```

```sql
  select * 
  from sys_user u 
```
现需要对其进行租户控制改造，让其只能查询本租户的数据：

```java
//别名 u 对应sql中需要进行逻辑隔离的表别名
@DataScope(eAlias = "u")
public List<SysUser> selectUserList(SysUser sysUser);
```

```sql
  select * 
  from sys_user u 
  <!-- 数据范围过滤 -->
  ${params.dataScope}     // dataScope 用于数据查询控制
```

* `ueAlias` 参数用于数据插入、更新、删除操作
  `ueAlias`参数有两种形式（`empty` or `other(别名)`）   

关于`ueAlias`使用，分为两部分，加`@DataScope`会执行`Id = 生成的雪花Id`,同时获取`creatBy`|`updateBy`|`enterpriseId`    
在`mybaties`中加入`${params.updateScope}`后，会进行如下释义    
`ueAlias`  sql释义：在更新sql后增加`（tenant_id = ?）`   

当需要进行租户隔离控制的sql没有别名时，使用`ueAlias = empty`,例如：

```sql
  update sys_user       //sys_user无别名
  where user_id = 1
```

当需要进行租户隔离控制的sql没有别名时，使用`ueAlias = 别名`,例如：

```sql
  update sys_user  u     //sys_user有别名 u , 此时ueAlias = u
  where u.user_id = 1
```

用法案例：在`SysUserMapper`中有一个`UpdateUser`方法，用于更新用户信息：

```java
public int updateUser(SysUser user);
```

```sql
  update sys_user
  set user_name = '雪忆' 
  where user_id = 1
```

现需要对其进行租户控制改造，让其只能更新属于本租户的数据：
1.当采用`empty`方式进行改造时：

```java
//empty 表示被逻辑隔离的表无别名
@DataScope(ueAlias = "empty")
public int updateUser(SysUser user);
```

```sql
  update sys_user
  set user_name = '雪忆' 
  where user_id = 1
  <!-- 数据范围过滤 -->
  ${params.updateScope}     // updateScope 用于数据更新|删除控制
```

2.当采用`other(别名)`方式进行改造时：

```java
//别名 u 对应sql中需要进行逻辑隔离的表别名
@DataScope(ueAlias = "u")
public int updateUser(SysUser user);
```

```sql
  update sys_user u
  set u.user_name = '雪忆' 
  where u.user_id = 1
  <!-- 数据范围过滤 -->
  ${params.updateScope}     // updateScope 用于数据更新|删除控制
```

::: tip 提示
仅实体继承BaseEntity才会进行处理，SQL语句会存放到`BaseEntity`对象中的`params`属性中供xml参数`params.dataScope`获取。   
当传递参数为非实体时可参考 `SysUserSeviceImpl` 与 `SysUserMapper` 中的使用通用查询对象的方式进行控制。    
:::

### 逻辑隔离-中级版
在本租户系统中，除了最初级的` tenant `隔离外，默认还可设置` library | site | system `三级控制：    

::: tip 友情提示
可以理解为系统分租户后，租户中间又可继续分子租户，子子租户
:::


` library | site | system `的数据隔离使用方法与 tenant 几乎一致，使用方法参考[逻辑隔离 - 初级版](/xueyi/document/htsc.html####逻辑隔离-初级版)
下面为` library | site | system `的使用别名及对应的表字段名
`SY - system - systemId - system_id` `SI - site - siteId - site_id` `LI - library - libraryId - library_id`

系统中为每个参数提供三个默认控制参数（以当前控制的Id `Id = 1` 为例）

| 参数                           | 类型                                   | 默认值                         | 描述
| ----------------------------- | ------------------------------------- | -----------------------------  | ----------------------------------------------------------------------------------------------------------------------------------
| SYAlias                        | String                                | 空                             | system表的别名（查询参数）（仅当前system）（例如：当前`systemId = 1`，通过本参数查询数据只能得到`systemId = 1`的数据）
| SYdAlias                       | String                                | 空                             | system+system基础表的别名（查询参数）（当前system数据+通用system数据）（例如：当前`systemId = 1`，通过本参数查询数据可以得到`systemId = 0 and systemId = 1`两个的数据）
| uSYAlias                       | String                                | 空                             | system更新控制的别名（更新参数）（仅当前system）（例如：当前`systemId = 1`，通过本参数只能更新`systemId = 1`的数据）

| 参数                           | 类型                                   | 默认值                         | 描述
| ----------------------------- | ------------------------------------- | -----------------------------  | ----------------------------------------------------------------------------------------------------------------------------------
| SIAlias                        | String                                | 空                             | site表的别名（查询参数）（仅当前site）（例如：当前`siteId = 1`，通过本参数查询数据只能得到`siteId = 1`的数据）
| SIdAlias                       | String                                | 空                             | site+site基础表的别名（查询参数）（当前site数据+通用site数据）（例如：当前`siteId = 1`，通过本参数查询数据可以得到`siteId = 0 and siteId = 1`两个的数据）
| uSIAlias                       | String                                | 空                             | site更新控制的别名（更新参数）（仅当前site）（例如：当前`siteId = 1`，通过本参数只能更新`siteId = 1`的数据）

| 参数                           | 类型                                   | 默认值                         | 描述
| ----------------------------- | ------------------------------------- | -----------------------------  | ----------------------------------------------------------------------------------------------------------------------------------
| LIAlias                        | String                                | 空                             | library表的别名（查询参数）（仅当前library）（例如：当前`libraryId = 1`，通过本参数查询数据只能得到`libraryId = 1`的数据）
| LIdAlias                       | String                                | 空                             | library+library基础表的别名（查询参数）（当前library数据+通用library数据）（例如：当前`libraryId = 1`，通过本参数查询数据可以得到`libraryId = 0 and libraryId = 1`两个的数据）
| uLIAlias                       | String                                | 空                             | library更新控制的别名（更新参数）（仅当前library）（例如：当前`libraryId = 1`，通过本参数只能更新`libraryId = 1`的数据）


其单个使用方法如[逻辑隔离 - 初级版](/xueyi/document/htsc.html###逻辑隔离-初级版)   

同时使用多个控制参数示例：   
```java
    @DataScope( ueAlias = "empty", uLIAlias = "empty", uSIAlias = "empty", uSYAlias = "empty" )    
    public int updateParameter(XyParameter parameter);
```


::: tip 提示
系统默认不会向后台传`systemId`、`siteId`、`libraryId`参数[使用方法示例](https://gitee.com/xueyitiantang/XueYi-Cloud/blob/master/xueyi-ui/common/src/api/menu.js)    
使用代码生成时，如若表中有相应的关键字段，即会自动添加相应的注解以及api中的参数上传方法。    
:::

### 逻辑隔离-进阶版
当需要调整或增加额外的控制参数时，可进行如下调控： 
* 增加控制方法
  进入[DataScopeAspect](https://gitee.com/xueyitiantang/XueYi-Cloud/blob/master/xueyi-common/xueyi-common-datascope/src/main/java/com/xueyi/common/datascope/aspect/DataScopeAspect.java)
增加切片方法     

* 代码生成增加控制
  进入[GenTable](https://gitee.com/xueyitiantang/XueYi-Cloud/blob/master/xueyi-modules/xueyi-gen/src/main/java/com/xueyi/gen/domain/GenTable.java)
添加控制参数对照即指定生成屏蔽

### 物理隔离

在实际开发中，经常可能遇到在一个应用中可能需要访问多个数据库的情况，微服务版本采用了`dynamic-datasource`动态多数据源组件，使用参考：

1、对应模块`pom`加入`xueyi-common-datasource`依赖
```xml
<!-- XueYi Common DataSource -->
<dependency>
    <groupId>com.xueyi</groupId>
    <artifactId>xueyi-common-datasource</artifactId>
</dependency>
```

2、以`xueyi-system`模块集成`druid`为例，配置主从数据库，其他数据源可以参考组件文档。
```yml
# spring配置
spring: 
  datasource:
    druid:
      stat-view-servlet:
        enabled: true
        loginUsername: admin
        loginPassword: 123456
    dynamic:
      druid:
        initial-size: 5
        min-idle: 5
        maxActive: 20
        maxWait: 60000
        timeBetweenEvictionRunsMillis: 60000
        minEvictableIdleTimeMillis: 300000
        validationQuery: SELECT 1 FROM DUAL
        testWhileIdle: true
        testOnBorrow: false
        testOnReturn: false
        poolPreparedStatements: true
        maxPoolPreparedStatementPerConnectionSize: 20
        filters: stat,wall,slf4j
        connectionProperties: druid.stat.mergeSql\=true;druid.stat.slowSqlMillis\=5000
      datasource:
          # 主库数据源
          master:
            driver-class-name: com.mysql.cj.jdbc.Driver
            url: jdbc:mysql://localhost:3306/xy-cloud?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
            username: root
            password: password
          # 上述配置中master不允许变更（可调整master的数据源信息）
```

3、`xueyi-common-datasource`定义数据源注解，对应`datasource`配置的不同数据源节点

项目默认通过上述配置获取主数据源信息，通过读取主数据源中数据库表的数据库信息，在系统初始化时进行自动加载配置。

关于数据源注解部分，系统暂提供六种方式调用指定数据源：
```java
-- ----------------------------
-- 1、产品规格表
-- ----------------------------
@DS("#main")                      //指定为主库
public List selectSpelBySession() {
	return userMapper.selectUsers();
}
@DS("#isolate")                   //指定为租户策略主数据源库 | 必须登录后才可生效
public List selectSpelBySession() {
	return userMapper.selectUsers();
}
@DS("#session.tenantName")        //从session获取
public List selectSpelBySession() {
	return userMapper.selectUsers();
}

@DS("#header.tenantName")         //从header获取
public List selectSpelByHeader() {
	return userMapper.selectUsers();
}

@DS("#tenantName")                //使用spel从参数获取
public List selectSpelByKey(String tenantName) {
	return userMapper.selectUsers();
}

@DS("#user.tenantName")           //使用spel从复杂参数获取
public List selecSpelByTenant(User user) {
	return userMapper.selectUsers();
}
```

::: tip 提示
使用注解在需要切换数据源的方法上或类上
:::


## 数据权限

在实际开发中，需要设置用户只能查看哪些部门的数据，这种情况一般称为数据权限。
又例如对于销售、财务等数据，它们是非常敏感的，因此要求对内部数据权限进行控制，
对于基于集团性的应用系统而言，就更多需要控制好各自公司的数据了。如设置只能看本公司、或者本部门的数据，对于特殊的领导，可能需要跨部门的数据，
因此程序不能硬编码那个领导该访问哪些数据，需要进行后台的权限和数据权限的控制。

::: tip 提示
默认每个租户的系统管理员`admin`拥有本租户内所有数据权限`（userType='00'）`
:::

::: tip 提示
关于数据权限使用流程
:::

### 注解参数说明

| 参数                           | 类型                                  | 默认值                         | 描述                                                        
| ------------------------------ | ------------------------------------- | ------------------------------ | ---------------------
| deptAlias                      | String                                | 空                             | 部门表的别名                                              
| postAlias                      | String                                | 空                             | 岗位表的别名                                               
| userAlias                      | String                                | 空                             | 用户表的别名                                                

数据权限仅用于控制本租户内的数据权限，租户与租户间的数据权限不生效。
1、在（系统管理-角色管理）设置需要数据权限的角色
目前支持以下几种权限
* 全部数据权限
* 自定数据权限
* 本部门数据权限
* 部门及以下数据权限
* 本岗位数据权限
* 仅本人数据权限

2、在需要数据权限控制方法上添加`@DataScope`注解，其中`d`、`p`、`u`用来表示表的别名
#### 部门数据权限注解
```java
@DataScope(deptAlias = "u")
public List<...> select(...){
    return mapper.select(...);
}
```
#### 部门及岗位权限注解
```java
@DataScope(deptAlias = "d", postAlias = "p")
public List<...> select(...){
    return mapper.select(...);
}
```
#### 部门、岗位及用户权限注解
```java
@DataScope(deptAlias = "d", postAlias = "p", userAlias = "u")
public List<...> select(...){
    return mapper.select(...);
}
```
3、在`mybatis`查询底部标签添加数据范围过滤
```sql
<select id="select" parameterType="..." resultMap="...Result">
    <include refid="select...Vo"/>
    <!-- 数据范围过滤 -->
    ${params.dataScope}
</select>
```

用户管理（未过滤数据权限的情况）：
```sql
select u.user_id, u.dept_id, u.login_name, u.user_name, u.email
	, u.phonenumber, u.password, u.sex, u.avatar, u.salt
	, u.status, u.del_flag, u.login_ip, u.login_date, u.create_by
	, u.create_time, u.remark, d.dept_name
from sys_user u
	left join sys_dept d on u.dept_id = d.dept_id
where u.del_flag = '0'
```

用户管理（已过滤数据权限的情况）：
```sql
select u.user_id, u.dept_id, u.login_name, u.user_name, u.email
	, u.phonenumber, u.password, u.sex, u.avatar, u.salt
	, u.status, u.del_flag, u.login_ip, u.login_date, u.create_by
	, u.create_time, u.remark, d.dept_name
from sys_user u
	left join sys_dept d on u.dept_id = d.dept_id
where u.del_flag = '0'
	and u.dept_id in (
		select dept_id
		from sys_role_dept
		where role_id = 2
	)
```

结果很明显，我们多了如下语句。通过角色部门表（sys_role_dept）完成了数据权限过滤
```sql
and u.dept_id in (
	select dept_id
	from sys_role_dept
	where role_id = 2
)
```

逻辑实现代码 `com.xueyi.common.datascope.aspectj.DataScopeAspect`

::: tip 提示
仅实体继承`BaseEntity`才会进行处理，SQL语句会存放到`BaseEntity`对象中的`params`属性中供xml参数`params.dataScope`获取。
:::





## 代码生成

大部分项目里其实有很多代码都是重复的，几乎每个基础模块的代码都有增删改查的功能，而这些功能都是大同小异，
如果这些功能都要自己去写，将会大大浪费我们的精力降低效率。所以这种重复性的代码可以使用代码生成。

### 默认配置

1、修改代码生成配置  
`nacos`配置中`xueyi-gen-dev.yml`中
```yaml
# 代码生成
gen: 
  # 开发者姓名，生成到类注释上
  author: xueyi
  # 默认生成包路径 system 需改成自己的模块名称 如 system monitor tool
  packageName: com.xueyi.system
  # 自动去除表前缀，默认是false
  autoRemovePre: true
  # 表前缀（生成类名不会包含表前缀，多个用逗号分隔）
  tablePrefix: sys_,xy_
```

### 单表结构
新建数据库表结构（单表）
```sql
drop table if exists sys_post;
create table sys_post (
  post_id                   bigint              not null                                comment '岗位Id',
  dept_id		            bigint	            not null                                comment '部门Id',
  post_code                 varchar(64)         default null                            comment '岗位编码',
  post_name                 varchar(50)         not null                                comment '岗位名称',
  sort                      int unsigned        not null default 0                      comment '显示顺序',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    varchar(1000)       default null                            comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志(0正常 1删除)',
  tenant_id		            bigint	            not null                                comment '租户Id',
  primary key (post_id)
) engine=innodb comment = '岗位信息表';
```
### 树表结构
新建数据库表结构（树表）
```sql
drop table if exists sys_dept;
create table sys_dept (
  dept_id                   bigint              not null                                comment '部门id',
  parent_id                 bigint              default 0                               comment '父部门id',
  dept_code                 varchar(64)         default null                            comment '部门编码',
  dept_name                 varchar(30)         default ''                              comment '部门名称',
  ancestors                 varchar(10000)      default ''                              comment '祖级列表',
  leader                    varchar(20)         default ''                              comment '负责人',
  phone                     varchar(11)         default ''                              comment '联系电话',
  email                     varchar(50)         default ''                              comment '邮箱',
  sort                      int unsigned        not null default 0                      comment '显示顺序',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    varchar(1000)       default null                            comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志(0正常 1删除)',
  tenant_id		            bigint	            not null                                comment '租户Id',
  primary key (dept_id)
) engine=innodb comment = '部门表';
```
### 主子表结构
新建数据库表结构（主子表）
```sql
-- ----------------------------
-- 产品规格表
-- ----------------------------
drop table if exists xy_mall_spec;
create table xy_mall_spec (
  spec_id		            bigint	            not null                                comment '规格Id',
  name		                varchar(100)	    not null	                            comment '规格名称',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  tenant_id		            bigint	            not null                                comment '租户Id',
  primary key (spec_id)
) engine=innodb comment = '产品规格表';

-- ----------------------------
-- 产品规格值表
-- ----------------------------
drop table if exists xy_mall_spec_value;
create table xy_mall_spec_value (
  spec_value_id		        bigint	            not null auto_increment                 comment '规格值Id',
  name		                varchar(100)	    not null	                            comment '规格值名称',
  image_url                 varchar(3000)	    default ''       	        	        comment '图片',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  spec_id		            bigint	            not null                                comment '规格Id',
  tenant_id		            bigint	            not null                                comment '租户Id',
  primary key (spec_value_id)
) engine=innodb auto_increment=100 comment = '产品规格值表';
```

::: tip 提示
XueYi-Cloud中代码生成的菜单默认租户Id均为0，请自行修改
:::

1、登录系统（系统工具 -> 代码生成 -> 导入对应表）

2、代码生成列表中找到需要表（可预览、修改、删除生成配置）

3、点击生成代码会得到一个`.zip`，按照包内目录结构复制到自己的项目中即可，同时复制`sql`文件内容，修改`sql`中的Id后再执行

::: tip 代码生成支持编辑、预览、同步

预览：对生成的代码提前预览，防止出现一些不符合预期的情况。     
同步：对原表的字段进行同步，包括新增、删除、修改的字段处理。    
修改：对生成的代码基本信息、字段信息、生成信息做一系列的调整。
所有代码生成的相关业务逻辑代码在`xueyi-gen`模块，可以自行调整或剔除。
:::
## 定时任务

在实际项目开发中Web应用有一类不可缺少的，那就是定时任务。
定时任务的场景可以说非常广泛，比如某些视频网站，购买会员后，每天会给会员送成长值，每月会给会员送一些电影券；
比如在保证最终一致性的场景中，往往利用定时任务调度进行一些比对工作；比如一些定时需要生成的报表、邮件；比如一些需要定时清理数据的任务等。
所以我们提供方便友好的web界面，实现动态管理任务，可以达到动态控制定时任务启动、暂停、重启、删除、添加、修改等操作，极大地方便了开发过程。

::: tip 提示
关于定时任务使用流程
:::

1、后台添加定时任务处理类（支持`Bean`调用、`Class`类调用）  
`Bean调用示例`：需要添加对应`Bean`注解`@Component`或`@Service`。调用目标字符串：`ryTask.ryParams('ry')`    
`Class类调用示例`：添加类和方法指定包即可。调用目标字符串：`com.xueyi.quartz.task.RyTask.ryParams('ry')`
```java
/**
 * 定时任务调度测试
 * 
 * @author xueyi
 */
@Component("ryTask")
public class RyTask{
    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i){
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params){
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams(){
        System.out.println("执行无参方法");
    }
}
```

2、前端新建定时任务信息（系统监控 -> 定时任务）  
任务名称：自定义，如：定时查询任务状态  
任务分组：根据字典`sys_job_group`配置  
调用目标字符串：设置后台任务方法名称参数  
执行表达式：可查询官方`cron`表达式介绍  
执行策略：定时任务自定义执行策略  
并发执行：是否需要多个任务间同时执行  
状态：是否启动定时任务  
备注：定时任务描述信息

3、点击执行一次，测试定时任务是否正常及调度日志是否正确记录，如正常执行表示任务配置成功。

执行策略详解：  
`立即执行`（所有`misfire`的任务会马上执行）打个比方，如果9点`misfire`了，在10：15系统恢复之后，9点，10点的`misfire`会马上执行  
`执行一次`（会合并部分的`misfire`，正常执行下一个周期的任务）假设9，10的任务都`misfire`了，系统在10：15分起来了。只会执行一次`misfire`，下次正点执行。    
`放弃执行`(所有的`misfire`不管，执行下一个周期的任务)

方法参数详解：  
`字符串`（需要单引号''标识 如：`ryTask.ryParams(’ry’)`）  
`布尔类型`（需要true false标识 如：`ryTask.ryParams(true)`）  
`长整型`（需要L标识 如：`ryTask.ryParams(2000L)`）  
`浮点型`（需要D标识 如：`ryTask.ryParams(316.50D)`）  
`整型`（纯数字即可）

cron表达式语法:  
[秒] [分] [小时] [日] [月] [周] [年]

| 说明  | 必填  | 允许填写的值       |	允许的通配符     |
| ----- | ----- | ------------------ | ----------------- |
| 秒    | 是    | 0-59               | , - * /           |
| 分    | 是    | 0-59               | , - * /           |
| 时    | 是    | 0-23               | , - * /           |
| 日    | 是    | 1-31               | , - * /           |
| 月    | 是    | 1-12 / JAN-DEC     | , - * ? / L W     |
| 周    | 是    | 1-7 or SUN-SAT     | , - * ? / L #     |
| 年    | 是    | 1970-2099          | , - * /           |

通配符说明:  
`*` 表示所有值。 例如:在分的字段上设置 *,表示每一分钟都会触发  
`?` 表示不指定值。使用的场景为不需要关心当前设置这个字段的值。例如:要在每月的10号触发一个操作，但不关心是周几，所以需要周位置的那个字段设置为”?” 具体设置为 0 0 0 10 * ?  
`-` 表示区间。例如 在小时上设置 “10-12”,表示 10,11,12点都会触发  
`,` 表示指定多个值，例如在周字段上设置 “MON,WED,FRI” 表示周一，周三和周五触发  
`/` 用于递增触发。如在秒上面设置”5/15” 表示从5秒开始，每增15秒触发(5,20,35,50)。 在月字段上设置’1/3’所示每月1号开始，每隔三天触发一次  
`L` 表示最后的意思。在日字段设置上，表示当月的最后一天(依据当前月份，如果是二月还会依据是否是润年[leap]), 在周字段上表示星期六，相当于”7”或”SAT”。如果在”L”前加上数字，则表示该数据的最后一个。例如在周字段上设置”6L”这样的格式,则表示“本月最后一个星期五”  
`W` 表示离指定日期的最近那个工作日(周一至周五). 例如在日字段上置”15W”，表示离每月15号最近的那个工作日触发。如果15号正好是周六，则找最近的周五(14号)触发, 如果15号是周未，则找最近的下周一(16号)触发.如果15号正好在工作日(周一至周五)，则就在该天触发。如果指定格式为 “1W”,它则表示每月1号往后最近的工作日触发。如果1号正是周六，则将在3号下周一触发。(注，”W”前只能设置具体的数字,不允许区间”-“)  
`#` 序号(表示每月的第几个周几)，例如在周字段上设置”6#3”表示在每月的第三个周六.注意如果指定”#5”,正好第五周没有周六，则不会触发该配置(用在母亲节和父亲节再合适不过了) ；小提示：’L’和 ‘W’可以一组合使用。如果在日字段上设置”LW”,则表示在本月的最后一个工作日触发；周字段的设置，若使用英文字母是不区分大小写的，即MON与mon相同

常用表达式例子:

| 表达式                         | 说明                                                        |
| ------------------------------ | ----------------------------------------------------------- |
| 0 0 2 1 * ? *                  | 表示在每月的1日的凌晨2点调整任务                            |
| 0 15 10 ? * MON-FRI            | 表示周一到周五每天上午10:15执行作业                         |
| 0 15 10 ? 6L 2002-2006         | 表示2002-2006年的每个月的最后一个星期五上午10:15执行作      |
| 0 0 10,14,16 * * ?             | 每天上午10点，下午2点，4点                                  |
| 0 0/30 9-17 * * ?              | 朝九晚五工作时间内每半小时                                  |
| 0 0 12 ? * WED                 | 表示每个星期三中午12点                                      |
| 0 0 12 * * ?                   | 每天中午12点触发                                            |
| 0 15 10 ? * *                  | 每天上午10:15触发                                           |
| 0 15 10 * * ?                  | 每天上午10:15触发                                           |
| 0 15 10 * * ? *                | 每天上午10:15触发                                           |
| 0 15 10 * * ? 2005             | 2005年的每天上午10:15触发                                   |
| 0 * 14 * * ?                   | 在每天下午2点到下午2:59期间的每1分钟触发                    |
| 0 0/5 14 * * ?                 | 在每天下午2点到下午2:55期间的每5分钟触发                    |
| 0 0/5 14,18 * * ?              | 在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发     |
| 0 0-5 14 * * ?                 | 在每天下午2点到下午2:05期间的每1分钟触发                    |
| 0 10,44 14 ? 3 WED             | 每年三月的星期三的下午2:10和2:44触发                        |
| 0 15 10 ? * MON-FRI            | 周一至周五的上午10:15触发                                   |
| 0 15 10 15 * ?                 | 每月15日上午10:15触发                                       |
| 0 15 10 L * ?                  | 每月最后一日的上午10:15触发                                 |
| 0 15 10 ? * 6L                 | 每月的最后一个星期五上午10:15触发                           |
| 0 15 10 ? * 6L 2002-2005       | 2002年至2005年的每月的最后一个星期五上午10:15触发           |
| 0 15 10 ? * 6#3                | 每月的第三个星期五上午10:15触发                             |

多模块所有定时任务的相关业务逻辑代码在`xueyi-quartz`模块，可以自行调整或剔除

`注意：不同数据源定时任务都有对应脚本，Oracle、Mysql已经有了，其他的可自行下载执行`

## 系统接口

在现在的开发过程中还有很大一部分公司都是以口口相传的方式来进行前后端的联调，而接口文档很大一部分都只停留在了说说而已的地步，或者写了代码再写文档。
还有一点就是文档的修改，定义好的接口并不是一成不变的，可能在开发过程中文档修改不止一次的变化，这个时候就会很难受了。
只要不是强制性要求，没人会愿意写这东西，而且在写的过程中，一个字母的错误就会导致联调时候的很大麻烦，但是通过`Swagger`，我们可以省略了这一步，而且文档出错率近乎于零，
只要你在写代码的时候，稍加几个注解，文档自动生成。

1、在控制层`Controller`中添加注解来描述接口信息如:
```java
@Api("参数配置")
@Controller
@RequestMapping("/system/config")
public class ConfigController
```

2、在方法中配置接口的标题信息
```sql
@ApiOperation("查询参数列表")
@ResponseBody
public TableDataInfo list(Config config){
	startPage();
	List<Config> list = configService.selectConfigList(config);
	return getDataTable(list);
}
```

3、在`系统工具-系统接口`测试相关接口

`注意：SwaggerConfig可以指定根据注解或者包名扫描具体的API`

`API`详细说明

| 作用范围                | API                                  |   使用位置                          |
| ----------------------- | ------------------------------------ | ----------------------------------- |
| 协议集描述              | @Api                                 |   用于controller类上                |
| 对象属性                | @ApiModelProperty                    |   用在出入参数对象的字段上          |
| 协议描述                | @ApiOperation                        |   用在controller的方法上            |
| Response集              | @ApiResponses                        |   用在controller的方法上            |
| Response                | @ApiResponse                         |   用在 @ApiResponses里边            |
| 非对象参数集            | @ApiImplicitParams                   |   用在controller的方法上            |
| 非对象参数描述          | @ApiImplicitParam                    |   用在@ApiImplicitParams的方法里边  |
| 描述返回对象的意义      | @ApiModel                            |   用在返回对象类上                  |


`api`标记，用在类上，说明该类的作用。可以标记一个`Controller`类做为`swagger`文档资源，使用方式：
```java
@Api(value = "/user", description = "用户管理")
```

与`Controller`注解并列使用。 属性配置：

| 属性名称              | 备注                                             |
| --------------------- | ------------------------------------------------ |
| value                 | url的路径值                                      |
| tags                  | 如果设置这个值、value的值会被覆盖                |
| description           | 对api资源的描述                                  |
| basePath              | 基本路径可以不配置                               |
| position              | 如果配置多个Api 想改变显示的顺序位置             |
| produces              | For example, "application/json, application/xml" |
| consumes              | For example, "application/json, application/xml" |
| protocols             | Possible values: http, https, ws, wss.           |
| authorizations        | 高级特性认证时配置                               |
| hidden                | 配置为true 将在文档中隐藏                        |

`ApiOperation`标记，用在方法上，说明方法的作用，每一个url资源的定义,使用方式：
```java
@ApiOperation("获取用户信息")
```

与`Controller`中的方法并列使用，属性配置：

| 属性名称              | 备注                                             |
| --------------------- | ------------------------------------------------ |
| value                 | url的路径值                                      |
| tags                  | 如果设置这个值、value的值会被覆盖                |
| description           | 对api资源的描述                                  |
| basePath              | 基本路径可以不配置                               |
| position              | 如果配置多个Api 想改变显示的顺序位置             |
| produces              | For example, "application/json, application/xml" |
| consumes              | For example, "application/json, application/xml" |
| protocols             | Possible values: http, https, ws, wss.           |
| authorizations        | 高级特性认证时配置                               |
| hidden                | 配置为true将在文档中隐藏                         |
| response              | 返回的对象                                       |
| responseContainer     | 这些对象是有效的 "List", "Set" or "Map".，其他无效            |
| httpMethod            | "GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS" and "PATCH" |
| code                  | http的状态码 默认 200                                         |
| extensions            | 扩展属性                                                      |

`ApiParam`标记，请求属性，使用方式：
```java
public TableDataInfo list(@ApiParam(value = "查询用户列表", required = true)User user)
```

与`Controller`中的方法并列使用，属性配置：

| 属性名称              | 备注                                             |
| --------------------- | ------------------------------------------------ |
| name                  | 属性名称                                         |
| value                 | 属性值                                           |
| defaultValue          | 默认属性值                                       |
| allowableValues       | 可以不配置                                       |
| required              | 是否属性必填                                     |
| access                | 不过多描述                                       |
| allowMultiple         | 默认为false                                      |
| hidden                | 隐藏该属性                                       |
| example               | 举例子                                           |

`ApiResponse`标记，响应配置，使用方式：
```java
@ApiResponse(code = 400, message = "查询用户失败")
```

与`Controller`中的方法并列使用，属性配置：

| 属性名称              | 备注                                             |
| --------------------- | ------------------------------------------------ |
| code                  | http的状态码                                     |
| message               | 描述                                             |
| response              | 默认响应类 Void                                  |
| reference             | 参考ApiOperation中配置                           |
| responseHeaders       | 参考 ResponseHeader 属性配置说明                 |
| responseContainer     | 参考ApiOperation中配置                           |

`ApiResponses`标记，响应集配置，使用方式:
```java
@ApiResponses({ @ApiResponse(code = 400, message = "无效的用户") })
```

与`Controller`中的方法并列使用，属性配置：

| 属性名称              | 备注                                             |
| --------------------- | ------------------------------------------------ |
| value                 | 多个ApiResponse配置                              |

`ResponseHeader`标记，响应头设置，使用方法
```java
@ResponseHeader(name="head",description="响应头设计")
```

与`Controller`中的方法并列使用，属性配置：

| 属性名称              | 备注                                             |
| --------------------- | ------------------------------------------------ |
| name                  | 响应头名称                                       |
| description           | 描述                                             |
| response              | 默认响应类 void                                  |
| responseContainer     | 参考ApiOperation中配置                           |

## 防重复提交
在接口方法上添加@RepeatSubmit注解即可，注解参数说明：

| 参数         | 类型          | 默认值                     | 描述                 
| ----------- | ------------ | ------------------------- | ----------------- 
| interval    | int          | 5000                      | 间隔时间(ms)，小于此时间视为重复提交             
| message     | String       | 不允许重复提交，请稍后再试     | 提示消息                       
**示例1：采用默认参数**
```java
@RepeatSubmit
public AjaxResult addSave(...){
    return success(...);
}
```
**示例2：指定防重复时间和错误消息**
```java
@RepeatSubmit(interval = 1000, message = "请求过于频繁")
public AjaxResult addSave(...){
    return success(...);
}
```
## 国际化支持

在我们开发WEB项目的时候，项目可能涉及到在国外部署或者应用，也有可能会有国外的用户对项目进行访问，那么在这种项目中，
为客户展现的页面或者操作的信息就需要使用不同的语言，这就是我们所说的项目国际化。
目前项目已经支持多语言国际化，接下来我们介绍如何使用。

### 后台国际化流程

1、修改`I18nConfig`设置默认语言，如默认`中文`：
```java
// 默认语言，英文可以设置Locale.US
slr.setDefaultLocale(Locale.SIMPLIFIED_CHINESE);
```

2、修改配置`application.yml`中的`basename`国际化文件，默认是`i18n`路径下`messages`文件  
（比如现在国际化文件是`xx_zh_CN.properties`、`xx_en_US.properties`，那么`basename`配置应为是`i18n/xx`
```yml
spring:
  # 资源信息
  messages:
    # 国际化资源文件路径
    basename: static/i18n/messages
```

3、`i18n`目录文件下定义资源文件  
美式英语 `messages_en_US.properties`
```properties
user.login.username=User name
user.login.password=Password
user.login.code=Security code
user.login.remember=Remember me
user.login.submit=Sign In
```
中文简体 `messages_zh_CN.properties`
```properties
user.login.username=用户名
user.login.password=密码
user.login.code=验证码
user.login.remember=记住我
user.login.submit=登录
```

4、`java`代码使用`MessageUtils`获取国际化
```java
MessageUtils.message("user.login.username")
MessageUtils.message("user.login.password")
MessageUtils.message("user.login.code")
MessageUtils.message("user.login.remember")
MessageUtils.message("user.login.submit")
```


### 前端国际化流程

1、`package.json`中`dependencies`节点添加`vue-i18n`
```json
"vue-i18n": "7.3.2",
```

2、`src`目录下创建`lang`目录，存放国际化文件  
此处包含三个文件，分别是 `index.js` `zh.js` `en.js`
```javascript
// index.js
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import elementZhLocale from 'element-ui/lib/locale/lang/zh-CN'// element-ui lang
import enLocale from './en'
import zhLocale from './zh'

Vue.use(VueI18n)

const messages = {
  en: {
    ...enLocale,
    ...elementEnLocale
  },
  zh: {
    ...zhLocale,
    ...elementZhLocale
  }
}

const i18n = new VueI18n({
  // 设置语言 选项 en | zh
  locale: Cookies.get('language') || 'en',
  // 设置文本内容
  messages
})

export default i18n
```

```javascript
// zh.js
export default {
  login: {
    title: '后台管理系统',
    logIn: '登录',
    username: '账号',
    password: '密码'
  },
  tagsView: {
    refresh: '刷新',
    close: '关闭',
    closeOthers: '关闭其它',
    closeAll: '关闭所有'
  },
  settings: {
    title: '系统布局配置',
    theme: '主题色',
    tagsView: '开启 Tags-View',
    fixedHeader: '固定 Header',
    sidebarLogo: '侧边栏 Logo'
  }
}
```

```javascript
// en.js
export default {
  login: {
    title: 'Login Form',
    logIn: 'Log in',
    username: 'Username',
    password: 'Password'
  },
  tagsView: {
    refresh: 'Refresh',
    close: 'Close',
    closeOthers: 'Close Others',
    closeAll: 'Close All'
  },
  settings: {
    title: 'Page style setting',
    theme: 'Theme Color',
    tagsView: 'Open Tags-View',
    fixedHeader: 'Fixed Header',
    sidebarLogo: 'Sidebar Logo'
  }
}
```

3、在`src/main.js`中增量添加`i18n`
```javascript
import i18n from './lang'

// use添加i18n
Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
})

new Vue({
  i18n,
})
```

4、在`src/store/getters.js`中添加`language`
```javascript
language: state => state.app.language
```

5、在`src/store/modules/app.js`中增量添加`i18n`
```javascript
const state = {
  language: Cookies.get('language') || 'en'
}

const mutations = {
  SET_LANGUAGE: (state, language) => {
    state.language = language
    Cookies.set('language', language)
  }
}

const actions = {
  setLanguage({ commit }, language) {
    commit('SET_LANGUAGE', language)
  }
}
```

6、在`src/components/LangSelect/index.vue`中创建汉化组件
```vue
<template>
  <el-dropdown trigger="click" class="international" @command="handleSetLanguage">
    <div>
      <svg-icon class-name="international-icon" icon-class="language" />
    </div>
    <el-dropdown-menu slot="dropdown">
      <el-dropdown-item :disabled="language==='zh'" command="zh">
        中文
      </el-dropdown-item>
      <el-dropdown-item :disabled="language==='en'" command="en">
        English
      </el-dropdown-item>
    </el-dropdown-menu>
  </el-dropdown>
</template>

<script>
export default {
  computed: {
    language() {
      return this.$store.getters.language
    }
  },
  methods: {
    handleSetLanguage(lang) {
      this.$i18n.locale = lang
      this.$store.dispatch('app/setLanguage', lang)
      this.$message({
        message: '设置语言成功',
        type: 'success'
      })
    }
  }
}
</script>
```

7、登录页面汉化
```vue
<template>
  <div class="login">
    <el-form ref="loginForm" :model="loginForm" :rules="loginRules" class="login-form">
      <h3 class="title">{{ $t('login.title') }}</h3>
      <lang-select class="set-language" />
      <el-form-item prop="username">
        <el-input v-model="loginForm.username" type="text" auto-complete="off" :placeholder="$t('login.username')">
          <svg-icon slot="prefix" icon-class="user" class="el-input__icon input-icon" />
        </el-input>
      </el-form-item>
      <el-form-item prop="password">
        <el-input
          v-model="loginForm.password"
          type="password"
          auto-complete="off"
          :placeholder="$t('login.password')"
          @keyup.enter.native="handleLogin"
        >
          <svg-icon slot="prefix" icon-class="password" class="el-input__icon input-icon" />
        </el-input>
      </el-form-item>
      <el-form-item prop="code">
        <el-input
          v-model="loginForm.code"
          auto-complete="off"
          placeholder="验证码"
          style="width: 63%"
          @keyup.enter.native="handleLogin"
        >
          <svg-icon slot="prefix" icon-class="validCode" class="el-input__icon input-icon" />
        </el-input>
        <div class="login-code">
          <img :src="codeUrl" @click="getCode" />
        </div>
      </el-form-item>
      <el-checkbox v-model="loginForm.rememberMe" style="margin:0px 0px 25px 0px;">记住密码</el-checkbox>
      <el-form-item style="width:100%;">
        <el-button
          :loading="loading"
          size="medium"
          type="primary"
          style="width:100%;"
          @click.native.prevent="handleLogin"
        >
          <span v-if="!loading">{{ $t('login.logIn') }}</span>
          <span v-else>登 录 中...</span>
        </el-button>
      </el-form-item>
    </el-form>
    <!--  底部  -->
    <div class="el-login-footer">
      <span>Copyright © 2020-2021 xueyi.cn All Rights Reserved.</span>
    </div>
  </div>
</template>

<script>
import LangSelect from '@components/LangSelect'
import { getCodeImg } from "@api/login";
import Cookies from "js-cookie";
import { encrypt, decrypt } from '@utils/jsencrypt'

export default {
  name: "Login",
  components: { LangSelect },
  data() {
    return {
      codeUrl: "",
      cookiePassword: "",
      loginForm: {
        username: "admin",
        password: "admin123",
        rememberMe: false,
        code: "",
        uuid: ""
      },
      loginRules: {
        username: [
          { required: true, trigger: "blur", message: "用户名不能为空" }
        ],
        password: [
          { required: true, trigger: "blur", message: "密码不能为空" }
        ],
        code: [{ required: true, trigger: "change", message: "验证码不能为空" }]
      },
      loading: false,
      redirect: undefined
    };
  },
  watch: {
    $route: {
      handler: function(route) {
        this.redirect = route.query && route.query.redirect;
      },
      immediate: true
    }
  },
  created() {
    this.getCode();
    this.getCookie();
  },
  methods: {
    getCode() {
      getCodeImg().then(res => {
        this.codeUrl = "data:image/gif;base64," + res.img;
        this.loginForm.uuid = res.uuid;
      });
    },
    getCookie() {
      const username = Cookies.get("username");
      const password = Cookies.get("password");
      const rememberMe = Cookies.get('rememberMe')
      this.loginForm = {
        username: username === undefined ? this.loginForm.username : username,
        password: password === undefined ? this.loginForm.password : decrypt(password),
        rememberMe: rememberMe === undefined ? false : Boolean(rememberMe)
      };
    },
    handleLogin() {
      this.$refs.loginForm.validate(valid => {
        if (valid) {
          this.loading = true;
          if (this.loginForm.rememberMe) {
            Cookies.set("username", this.loginForm.username, { expires: 30 });
            Cookies.set("password", encrypt(this.loginForm.password), { expires: 30 });
            Cookies.set('rememberMe', this.loginForm.rememberMe, { expires: 30 });
          } else {
            Cookies.remove("username");
            Cookies.remove("password");
            Cookies.remove('rememberMe');
          }
          this.$store
            .dispatch("Login", this.loginForm)
            .then(() => {
              this.loading = false;
              this.$router.push({ path: this.redirect || "/" });
            })
            .catch(() => {
              this.loading = false;
              this.getCode();
            });
        }
      });
    }
  }
};
</script>

<style rel="stylesheet/scss" lang="scss">
.login {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  background-image: url("../assets/image/login-background.jpg");
  background-size: cover;
}
.title {
  margin: 0px auto 30px auto;
  text-align: center;
  color: #707070;
}

.login-form {
  border-radius: 6px;
  background: #ffffff;
  width: 400px;
  padding: 25px 25px 5px 25px;
  .el-input {
    height: 38px;
    input {
      height: 38px;
    }
  }
  .input-icon {
    height: 39px;
    width: 14px;
    margin-left: 2px;
  }
}
.login-tip {
  font-size: 13px;
  text-align: center;
  color: #bfbfbf;
}
.login-code {
  width: 33%;
  height: 38px;
  float: right;
  img {
    cursor: pointer;
    vertical-align: middle;
  }
}
.el-login-footer {
  height: 40px;
  line-height: 40px;
  position: fixed;
  bottom: 0;
  width: 100%;
  text-align: center;
  color: #fff;
  font-family: Arial;
  font-size: 12px;
  letter-spacing: 1px;
}
</style>
```

```
普通文本使用方式： {{ $t('login.title') }}
标签内使用方式：   :placeholder="$t('login.password')"
js内使用方式       this.$t('login.user.password.not.match')
```


## 新建子模块

`Maven`多模块下新建子模块流程案例。

1、在`xueyi-modules`下新建业务模块目录，例如：`xueyi-test`。

2、在`xueyi-test`业务模块下新建`pom.xml`文件以及`src\main\java`，`src\main\resources`目录。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>com.xueyi</groupId>
        <artifactId>xueyi-modules</artifactId>
        <version>x.x.x</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>
	
    <artifactId>xueyi-modules-test</artifactId>

    <description>
      xueyi-modules-test系统模块
    </description>
	
    <dependencies>
    	
    	<!-- SpringCloud Ailibaba Nacos -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        
        <!-- SpringCloud Ailibaba Nacos Config -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        
    	<!-- SpringCloud Ailibaba Sentinel -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>

        <!-- SpringBoot Actuator -->
        <dependency>
          <groupId>org.springframework.boot</groupId>
          <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
		
        <!-- Mysql Connector -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
        
        <!-- XueYi Common Security -->
        <dependency>
            <groupId>com.xueyi</groupId>
            <artifactId>xueyi-common-security</artifactId>
        </dependency>
        
        <!-- XueYi Common Log -->
        <dependency>
            <groupId>com.xueyi</groupId>
            <artifactId>xueyi-common-log</artifactId>
        </dependency>
        
        <!-- XueYi Common Swagger -->
        <dependency>
            <groupId>com.xueyi</groupId>
            <artifactId>xueyi-common-swagger</artifactId>
        </dependency>

        <!-- 无需使用多数据源则可移除 -->
        <!-- XueYi Common DataSource -->
        <dependency>
          <groupId>com.xueyi</groupId>
          <artifactId>xueyi-common-datasource</artifactId>
        </dependency>
      
        <!-- 无需使用数据权限或租户控制则可移除 -->
        <!-- XueYi Common DataScope -->
        <dependency>
          <groupId>com.xueyi</groupId>
          <artifactId>xueyi-common-datascope</artifactId>
        </dependency>
      
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
   
</project>
```

3、在`xueyi-modules`目录下`pom.xml`模块节点`modules`添加业务模块

```xml
<module>xueyi-test</module>
```

4、`src/main/resources`添加`bootstrap.yml`文件

```yml
# Tomcat
server:
  port: 9301

# Spring
spring: 
  application:
    # 应用名称
    name: xueyi-test
  main:
    allow-bean-definition-overriding: true #当遇到同样名字的时候，是否允许覆盖注册
  profiles:
    # 环境配置
    active: dev
  cloud:
    nacos:
      discovery:
        # 服务注册地址
        server-addr: 127.0.0.1:8848
      config:
        # 配置中心地址
        server-addr: 127.0.0.1:8848
        # 配置文件格式
        file-extension: yml
        # 共享配置
        shared-configs:
          - application-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
          - application-secret-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
          # 无需使用动态多数据源则移除
          - application-datasource-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
```

5、`com.xueyi.test`包下添加启动类

```java
package com.xueyi.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.xueyi.common.security.annotation.EnableCustomConfig;
import com.xueyi.common.security.annotation.EnableRyFeignClients;
import com.xueyi.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 测试模块
 * 
 * @author xueyi
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class XueYiTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(XueYiTestApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  测试模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  _____     __   ____     __        \n" +
                "  \\   _\\   /  /  \\   \\   /  /   \n" +
                "  .-./ ). /  '    \\  _. /  '       \n" +
                "  \\ '_ .') .'      _( )_ .'        \n" +
                " (_ (_) _) '   ___(_ o _)'          \n" +
                "   /    \\   \\ |   |(_,_)'         \n" +
                "   `-'`-'    \\|   `-'  /           \n" +
                "  /  /   \\    \\\\      /          \n" +
                " '--'     '----'`-..-'              ");
    }
}
```
