# 组件文档

系统使用到的相关组件

## 基础框架组件

[element-ui](https://github.com/ElemeFE/element)

[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)

## 树形选择组件

[vue-treeselect](https://github.com/riophae/vue-treeselect)

## 富文本编辑器

[quill](https://github.com/quilljs/quill)

## 表格分页组件

[pagination](https://gitee.com/y_project/RuoYi-Vue/blob/master/ruoyi-ui/src/components/Pagination/index.vue)

## 富文本组件

[editor](https://gitee.com/y_project/RuoYi-Vue/blob/master/ruoyi-ui/src/components/Editor/index.vue)

## 工具栏右侧组件

[right-toolbar](https://gitee.com/y_project/RuoYi-Vue/blob/master/ruoyi-ui/src/components/RightToolbar/index.vue)

## 表单设计组件

[form-generator](https://github.com/JakHuang/form-generator)

## 排序组件

[SortableJS](https://github.com/SortableJS/Sortable)
