# 环境部署

## 准备工作

~~~
JDK >= 1.8 (推荐1.8版本)
Mysql >= 5.7.0 (推荐8.0+版本)
Redis >= 3.0
Maven >= 3.0
Node >= 10
nacos >= 2.0.0
rabbitMQ >= 3.8 (推荐3.8+版本)
sentinel >= 1.6.0
~~~


## 运行系统

::: tip 提示
因为本项目是前后端分离的，所以需要前后端都启动好，才能进行访问
:::

前往Gitee下载页面([https://gitee.com/xueyitiantang/XueYi-Cloud](https://gitee.com/xueyitiantang/XueYi-Cloud))下载解压到工作目录  

### 后端运行
1、导入到`Eclipse`，菜单 `File` -> `Import`，然后选择 `Maven` -> `Existing Maven Projects`，点击 `Next>` 按钮，选择工作目录，然后点击 `Finish` 按钮，即可成功导入Eclipse会自动加载Maven依赖包，初次加载会比较慢（根据自身网络情况而定）  
2、创建数据库`xy-config`并导入数据脚本`xy_config.sql`（<font color=#FF0000>必须</font>）  
3、启动`nacos`，修改配置文件中数据库连接信息（<font color=#FF0000>必须</font>）  
4、修改`xueyi_1.sql`中数据源表`xy_tenant_source`中的数据源连接记录（<font color=#FF0000>修改成自己用的</font>），其中，`master`库为主库，导入数据脚本`xueyi_1.sql`；`slave`库对应为默认从库，导入数据脚本`xueyi_2.sql`（当`master库`和`slave库`可为同一个库）（<font color=#8A8A8A>必须</font>）  
5、`master`库导入数据脚本`quartz.sql`（<font color=#8A8A8A>可选</font>）  
6、配置`nacos`持久化，修改`conf/application.properties`文件，增加支持`mysql`数据源配置           
```yml
# db mysql
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://localhost:3306/xy-config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=root
db.password=password
```
::: tip 提示
如果还是有疑惑，这里有[启动视频](https://space.bilibili.com/479745149)，自己去看一下。             
配置文件`application.properties`是在下载的`nacos-server`包`conf`目录下。              
默认配置单机模式，`nacos`也集群/多集群部署模式参考 ([Nacos支持三种部署模式](https://nacos.io/zh-cn/docs/deployment.html))        
运行前需要先启动`nacos`，运行成功可以通过([http://localhost:8080](http://localhost:8080))访问，但是不会出现静态页面，可以继续参考下面步骤部署`xueyi-ui`前端，然后通过前端地址来访问。         
:::
7、启动`nacos`，登录`nacos管理页面`，修改`application-secret-dev.yml`中的redis等连接信息      
8、打开运行基础模块（启动没有先后顺序）
* XueYiGatewayApplication （网关模块 <font color=#FF0000>必须</font>）
* XueYiAuthApplication    （认证模块 <font color=#FF0000>必须</font>）
* XueYiSystemApplication  （系统模块 <font color=#FF0000>必须</font>）
* XueYiTenantApplication  （租管模块 <font color=#8A8A8A>可选</font>）
* XueYiMonitorApplication （监控中心 <font color=#8A8A8A>可选</font>）
* XueYiGenApplication     （代码生成 <font color=#8A8A8A>可选</font>）
* XueYiJobApplication     （定时任务 <font color=#8A8A8A>可选</font>）
* XueYiFileApplication    （文件服务 <font color=#8A8A8A>可选</font>）

6、集成`seata`分布式事务（可选配置，默认不启用）    

创建数据库`xy-seata`并导入数据脚本`xy_seata.sql`    

::: tip 提示
1、当修改库`xy-config`中表`config_info`中的主数据库信息时（`nacos`中`application-secret-dev.yml`的`master`数据库信息），请同步修改库`xy-cloud`中表`xy_tenant_source`中关于`默认数据源`的数据库连接信息           
2、物理隔离多库下，`master`主库导入数据脚本`xueyi_1.sql`、`slave`从库导入`xueyi_2.sql`，后续新增子数据库只需要导入数据脚本`xueyi_3.sql`     
:::

### 前端运行

```bash
# 进入项目目录
cd xueyi-ui

# 依赖安装
# 方式1
yarn install && npm-run-all install:*
# 方式2
yarn install; npm run install-all
# 方式3
npm install
cd main && npm install
cd administrator && npm install

# 只安装各模块依赖
npm-run-all install:*

# 启动服务
# 方式1
npm-run-all --parallel start:*
# 方式2
npm run start-all
# 方式3
cd main && vue-cli-service serve
cd administrator && vue-cli-service serve
```

4、打开浏览器，输入：http://localhost:80 （普通账户 `xueYi/admin/admin123`）  （超管账户`administrator/admin/admin123`）
若能正确展示登录页面，并能成功登录，菜单及页面展示正常，则表明环境搭建成功  

建议使用`Git`克隆，因为克隆的方式可以和`XueYi-Cloud`随时保持更新同步。使用`Git`命令克隆

```
git clone https://gitee.com/xueyitiantang/XueYi-Cloud.git
```

::: tip 提示
因为本项目是前后端完全分离的，所以需要前后端都单独启动好，才能进行访问。
前端安装完node后，最好设置下淘宝的镜像源，不建议使用cnpm（可能会出现奇怪的问题）
:::


## 部署系统

::: tip 提示
因为本项目是前后端分离的，所以需要前后端都部署好，才能进行访问
:::

### 后端部署

* 打包工程文件

在`xueyi`项目的bin目录下执行`package.bat`打包Web工程，生成war/jar包文件。    
然后会在项目下生成`target`文件夹包含war或jar

::: tip 提示
打包时建议通过`idea | Eclipse`的`Maven`进行打包，`package.bat`偶尔会出现莫名bug
不同模块版本会生成在`xueyi/xueyi-xxxx`模块下`target`文件夹
:::

* 部署工程文件

1、jar部署方式  
使用命令行执行：`java –jar xxxx.jar` 或者执行脚本：`bin/run.bat`  

2、war部署方式  
`xueyi/pom.xml`中的`packaging`修改为`war`，放入`tomcat`服务器`webapps`

```xml
   <packaging>war</packaging>
```

::: tip 提示
不同模块版本在`xueyi/xueyi-xxxx`模块下修改`pom.xml`
:::

* `SpringBoot`去除内嵌`Tomcat`（PS：此步骤不重要，因为不排除也能在容器中部署`war`）

```xml
<!-- 排除内置tomcat -->
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
   <exclusions>
      <exclusion>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-tomcat</artifactId>
      </exclusion>
   </exclusions>
</dependency>
```

### 前端部署

当项目开发完毕，只需要运行一行命令就可以打包你的应用

```bash
# 打包主模块的生产环境
cd main && vue-cli-service build

# 打包租户管理模块的生产环境
cd administrator && vue-cli-service build

# 打包全部模块的生产环境 | 可以运行npm 脚本 的build-all（执行它则无需执行上述两个）
concurrently "cd administrator && vue-cli-service build" "cd main && vue-cli-service build"
```

构建打包成功之后，会在对应模块的根目录生成 `dist` 文件夹，里面就是构建打包好的文件，通常是 `***.js` 、`***.css`、`index.html` 等静态文件。

通常情况下 `dist` 文件夹的静态文件发布到你的 nginx 或者静态服务器即可，其中的 `index.html` 是后台服务的入口页面。

::: tip outputDir 提示
如果需要自定义构建，比如指定 `dist` 目录等，则需要通过 [config](https://gitee.com/xueyitiantang/XueYi-Cloud/blob/master/xueyi-ui/main/vue.config.js)的 `outputDir` 进行配置。
:::

::: tip publicPath 提示
部署时改变页面js 和 css 静态引入路径 ,只需修改 `vue.config.js` 文件资源路径即可。
:::

```js
publicPath: './' //请根据自己路径来配置更改
```

```js
export default new Router({
  mode: 'hash', // hash模式
})
```

## 环境变量

所有测试环境或者正式环境变量的配置都在 [.env.development](https://gitee.com/xueyitiantang/XueYi-Cloud/blob/master/xueyi-ui/main/.env.development)等 `.env.xxxx`文件中。

它们都会通过 `webpack.DefinePlugin` 插件注入到全局。

::: tip 注意！！！
环境变量必须以`VUE_APP_`为开头。如:`VUE_APP_API`、`VUE_APP_TITLE`

你在代码中可以通过如下方式获取:

```js
console.log(process.env.VUE_APP_xxxx)
```

:::

<br>

## Nginx配置

```nginx
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;

    server {
        listen       80;
        server_name  localhost;

		location / {
            root   /home/xueyi/projects/xueyi-ui;
			try_files $uri $uri/ /index.html;
            index  index.html index.htm;
        }
		
		location /prod-api/{
			proxy_set_header Host $http_host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header REMOTE-HOST $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_pass http://localhost:8080/;
		}

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}
```

## Tomcat配置

修改`server.xml`，`Host`节点下添加
```xml
<Context docBase="" path="/" reloadable="true" source=""/>
```

`dist`目录的文件夹下新建`WEB-INF`文件夹，并在里面添加`web.xml`文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
        http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1" metadata-complete="true">
   <display-name>Router for Tomcat</display-name>
   <error-page>
      <error-code>404</error-code>
      <location>/index.html</location>
   </error-page>
</web-app>
```

## 常见问题

1. 如果使用`Mac`需要修改`application.yml`文件路径`profile`
2. 如果使用`Linux`提示表不存在，设置大小写敏感配置在`/etc/my.cnf`添加`lower_case_table_names=1`，重启`MYSQL`服务
3. 如果提示当前权限不足，无法写入文件请检查`application.yml`中的`profile`路径或`logback.xml`中的`log.path`路径是否有可读可写操作权限

如遇到无法解决的问题请到[Issues](https://gitee.com/xueyitiantang/XueYi-Cloud/issues)反馈，会不定时进行解答。
