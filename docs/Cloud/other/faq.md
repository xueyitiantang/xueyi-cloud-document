# 常见问题

## 如何新增系统图标

如果你没有在本项目 [Icon](https://gitee.com/xueyitiantang/XueYi-Cloud/tree/master/xueyi-ui/common/src/assets/icons/svg) 中找到需要的图标，可以到 [iconfont.cn](http://iconfont.cn/) 上选择并生成自己的业务图标库，再进行使用。或者其它 svg 图标网站，下载 svg 并放到文件夹之中就可以了。

下载完成之后将下载好的 .svg 文件放入 `@/icons/svg` 文件夹下之后就会自动导入。

**使用方式**

```js
<svg-icon icon-class="password" /> // icon-class 为 icon 的名字
```

:::tip 提示
菜单图标会自动引入`@/icons/svg`，放入此文件夹中图标就可以选择了
:::


## 如何不登录直接访问
登录`nacos`在`配置管理`中`配置列表`，修改`xueyi-gateway-dev.yml`

在 `ignore` 中设置`whites`，表示允许匿名访问

```yml
# 不校验白名单
security:
  ignore:
    whites:
      - /auth/logout
      - /auth/login
      - /*/v2/api-docs
      - /csrf
```
## 如何判断当前租户是否为租管租户
```java
// true 租管租户 false 普通租户

// 1.未登录状态
Long enterpriseId = xx;
EnterpriseUtils.isAdminTenant(enterpriseId)

// 2.已登录状态
SecurityUtils.isAdminTenant()
```
## 如何判断当前用户是否为超管用户
```java
// true 超管用户 false 普通用户
SecurityUtils.isAdminUser()
```
## 如何获取用户登录信息
1. 后端获取当前用户信息
```java
// 获取当前企业Id
Long enterpriseId = SecurityUtils.getEnterpriseId();
// 获取当前的企业账号
String enterpriseName = currentUser.getEnterpriseName();
// 获取当前用户类型
String isLessor = SecurityUtils.getIsLessor();

// 获取当前用户Id
Long userId = SecurityUtils.getUserId();
// 获取当前用户账号
String userName = SecurityUtils.getUserName();
// 获取当前用户类型
String userType = SecurityUtils.getUserType();

// 获取当前的用户信息
@Autowired
private TokenService tokenService;

LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
```
2、vue中获取当前用户信息
```javascript
var userName = this.$store.state.user.name;
```
## 如何根据租户Id获取租户对应的主数据源
```java
Long enterpriseId = xx;
String mainSource = DataSourceUtils.getMainSourceNameByEnterpriseId(enterpriseId);
```
## 提示您没有数据的权限

这种情况都属于权限标识配置不对在```菜单管理```配置好权限标识（菜单&按钮）
1. 确认此用户是否已经配置角色
2. 确认此角色是否已经配置菜单权限
3. 确认此菜单权限标识是否和后台代码一致  

如参数管理  
后台配置```@RequiresPermissions('system:config:query')")```对应参数管理权限标识为```system:config:query```

注：如需要角色权限，配置角色权限字符 使用```@PreAuthorize(hasRole = "admin")```
## 登录页面如何不显示验证码
登录`nacos`在配置管理中配置列表，修改`xueyi-gateway-dev.yml`

在`captcha`中设置`enabled`属性（`true`开启、`false`关闭）
```yaml
# 验证码
security:
  captcha:
    enabled: false
```
## 特殊字符串被过滤的解决办法
默认所有的都会过滤脚本，可以在`xueyi-gateway-dev.yml`配置`xss.excludeUrls`属性排除URL
```yaml
# 安全配置
security:
  xss:
    enabled: true
    excludeUrls:
      - /system/notice
```
## 如何格式化前端日期时间戳
对应一些时间格式需要在前端进行格式化操作情况，解决方案如下

1、后端使用`JsonFormat`注解格式化日期，时间戳`yyyy-MM-dd HH:mm:ss`
```java
/** 创建时间 */
@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
private Date time;
```
2、前端使用`parseTime`方法格式化日期，时间戳`{y}-{m}-{d} {h}:{i}:{s}`
```javascript
<el-table-column label="创建时间" align="center" prop="createTime" width="160">
	<template slot-scope="scope">
	  <span>{{ parseTime(scope.row.createTime, '{y}-{m}-{d} {h}:{i}:{s}') }}</span>
	</template>
</el-table-column>
```
