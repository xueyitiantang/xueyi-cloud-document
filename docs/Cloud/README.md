# 介绍

<h1 align="center">XueYi-Cloud</h1>

<p align="center">
    <a>
       <img src="https://img.shields.io/badge/XueYi%20Cloud-v4.1.3-brightgreen" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/SpringBoot-2.5.6-brightgreen" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/Vue2-Element%20UI-green" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/Spring%20Cloud%20%26%20Alibaba-2021.1-brightgreen" alt="xueYi-cloud">
    </a>
    <a>
       <img src="https://img.shields.io/badge/Mybatis-2.2.0-brightgreen" alt="xueYi-cloud">
    </a>
</p>

**XueYi-Cloud | 雪忆微服务多租户Saas** 是一个 Java EE 企业级微服务SaaS开发框架，基于经典技术组合（SpringBoot | Spring Cloud & Alibaba | Mybatis | RabbitMQ | Vue2 | Element UI），内置模块如：租户管理、租户源策略管理、数据源管理、机构管理（部门-岗位-用户）、权限管理（模块-菜单-角色）、素材管理中心、系统参数、日志管理、代码生成等。在线定时任务配置；支持集群，支持多租户，支持动态源，数据物理&逻辑双隔离。

**在线体验**
1. 项目地址：[https://xueyitt.cn](https://xueyitt.cn)
2. 普通账号：xueYi/admin/admin123
2. 租管账号：administrator/admin/admin123

**系统需求**

- JDK >= 1.8
- MySQL >= 8.0
- Maven >= 3.0
- RabbitMQ >= 3.0

**技术交流群**
- [![加入QQ群](https://img.shields.io/badge/779343138-blue.svg)](https://jq.qq.com/?_wv=1027&k=zw11JJhj)
