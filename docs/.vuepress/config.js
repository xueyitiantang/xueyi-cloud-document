module.exports = {
	title : 'XueYi',
	description : '快速构建web应用程序',
	port : 3000,
	dest : "dist",
	head : [
		[
			'link', {
				rel : 'icon',
				href : '/images/favicon.ico'
			}

		],
		[
			'link', {
				rel : 'stylesheet',
				href : '/css/ruoyi.css'
			}
		],
		[
			'script', {
				charset : 'utf-8',
				src : '/js/ruoyi.js'
			}
		]
	],
	base : '',
	markdown : {
		lineNumbers : true // 代码块是否显示行号
	},
	themeConfig : {
		sidebarDepth : 3,
		/*
		algolia : {
			// 使用官方注册key无需appId
			appId: 'YDP9KZMGO2',
			// 官方注册默认 key 017ae7acde5e01882ca2985797787d06
			apiKey : 'cc3949f0409f5b9067b49292194b5bfe',
			indexName : 'ruoyi',
			algoliaOptions : {
				hitsPerPage : 5,
				facetFilters : ""
			}
		},
		*/
		nav : [// 导航栏配置
			{
				text : '多租户版',
				link : '/Cloud/'
			}, {
				text : '多租户MP版',
				link : '/MultiSaas/'
			}, {
				text : 'Gitee',
				link : 'https://gitee.com/xueyitiantang/XueYi-Cloud'
			}
		],
		sidebar : {
			'/Cloud/' : [{
					title : '文档',
					collapsable : false,
					children : [
						'',
						'document/kslj',
						'document/hjbs',
						'document/xmjs',
						'document/htsc',
						'document/qdsc',
						'document/zjwd',
						'document/gxrz'
					]
				},
				{
					title : '微服务',
					collapsable : false,
					children : [
						'microservice/docker'
					]
				},
				{
					title : '其它',
					collapsable : false,
					children : [
						'other/faq'
					]
				}
			],
			'/MultiSaas/' : [{
				title : '文档',
				collapsable : false,
				children : [
					'',
					'document/base',
				]
			}]
		}
	}
};
